<?php

namespace App\Controller;
use App\Repository\LivreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;


#[Route('/api/livre')]
class LivreController extends AbstractController
{
    private LivreRepository $repo;
    /**
     * On utilise ici l'injection de dépendance de Symfony qui va, en se basant sur le type de
     * l'argument, se charger de créer une instance de la classe voulue
     * On peut aussi faire de l'injection dans les méthodes directement, chose qu'on fait en dessous
     * par exemple avec la Request et le SerializerInterface (ces deux classes spécifiques, on ne sait
     * même pas trop comment en faire des instances nous même, mais Symfony lui le sait, et s'en charge)
     */
    public function __construct(LivreRepository $repo)
    {
        $this->repo = $repo;
    }
 

    #[Route(methods: 'GET')]
    public function all()
    {

        $livres = $this->repo->findAll();
        return $this->json($livres);
    }


    #[Route('/{id}', methods: 'GET')]
    public function one(int $id) {
        $livre = $this->repo->findById($id);
        if(!$livre){
            throw new NotFoundHttpException();
        }
        return $this->json($livre);
    }

    
    #[Route('/{id}', methods: 'DELETE')]
    public function delete(Request $request, SerializerInterface $serializer, int $id)
    {

        $livre = $this->repo->findById($id);
        if (!$livre) {
            return $this->json(['message' => 'Book not found'], Response::HTTP_NOT_FOUND);
        }
        $this->repo->delete($livre);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }


    // #[Route(methods: 'POST')]
    // public function add(Request $request, SerializerInterface $serializer): JsonResponse {
    //     $article = $serializer->deserialize($request->getContent(), Article::class, 'json');
        
    //     $this->repo->persist($article);

    //     return $this->json($article, Response::HTTP_CREATED);

    // }

    // #[Route('/{id}', methods: 'PUT')]
    // public function put(int $id, Request $request, SerializerInterface $serializer) {   
    //     $article = $this->repo->findById($id);
    //     if(!$article){
    //         throw new NotFoundHttpException();
    //     }

    //     $toUpdate = $serializer->deserialize($request->getContent(), Article::class, 'json');
    //     $toUpdate->setId($id);
    //     $this->repo->update($toUpdate);

    //     return $this->json($toUpdate);
    // }


        // #[Route('/{id}', methods: 'PATCH')]
        // public function patch(Request $request, SerializerInterface $serializer, int $id)
        // {
        //     $article = $this->repo->findById($id);
        //     if (!$article) {
        //         return $this->json(['message' => 'Article not found'], Response::HTTP_NOT_FOUND);
        //     }
            
        //     $data = json_decode($request->getContent(), true);
        //     if (isset($data['img'])) {
        //         $article->setImg($data['img']);
        //     }
        //     if (isset($data['title'])) {
        //         $article->setTitle($data['title']);
        //     }
        //     if (isset($data['content'])) {
        //         $article->setContent($data['content']);
        //     }
        //     if (isset($data['date'])) {
        //         $article->setDate($data['date']);
        //     }
        //     if (isset($data['author'])) {
        //         $article->setAuthor($data['author']);
        //     }
        //     if (isset($data['idCategory'])) {
        //         $article->setIdCategory($data['idCategory']);
        //     }
            
        
        //     $this->repo->update($article);
        //     return $this->json($article, Response::HTTP_OK);
        // }


}