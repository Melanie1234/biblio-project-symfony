<?php

namespace App\Entities;
use DateTime;

class Livre{
    private ?int $id;
    private string $titre;
    private string $auteur;

    private DateTime $date;
    private bool $dispo;

	

    /**
     * @param int|null $id
     * @param string $titre
     * @param string $auteur
     * @param DateTime $date
     * @param bool $dispo
     */
    public function __construct(string $titre, string $auteur, DateTime $date, bool $dispo, ?int $id =null) {
    	$this->id = $id;
    	$this->titre = $titre;
    	$this->auteur = $auteur;
    	$this->date = $date;
    	$this->dispo = $dispo;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getTitre(): string {
		return $this->titre;
	}
	
	/**
	 * @param string $titre 
	 * @return self
	 */
	public function setTitre(string $titre): self {
		$this->titre = $titre;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getAuteur(): string {
		return $this->auteur;
	}
	
	/**
	 * @param string $auteur 
	 * @return self
	 */
	public function setAuteur(string $auteur): self {
		$this->auteur = $auteur;
		return $this;
	}
	
	/**
	 * @return DateTime
	 */
	public function getDate(): DateTime {
		return $this->date;
	}
	
	/**
	 * @param DateTime $date 
	 * @return self
	 */
	public function setDate(DateTime $date): self {
		$this->date = $date;
		return $this;
	}
	
	/**
	 * @return bool
	 */
	public function getDispo(): bool {
		return $this->dispo;
	}
	
	/**
	 * @param bool $dispo 
	 * @return self
	 */
	public function setDispo(bool $dispo): self {
		$this->dispo = $dispo;
		return $this;
	}
}